%define real_version 6.4.0
%define short_version 6.4
%define tar_name qt5compat-everywhere-src
%define tar_suffix %{nil}
Name:           qt6-qt5compat
Version:        6.4.0
Release:        1
Summary:        Unsupported Qt 5 APIs for Qt 6
License:        LGPL-3.0-only OR (GPL-2.0-only OR GPL-3.0-or-later)
URL:            https://www.qt.io
Source:         https://download.qt.io/official_releases/qt/%{short_version}/%{real_version}%{tar_suffix}/submodules/%{tar_name}-%{real_version}%{tar_suffix}.tar.xz
BuildRequires:  fdupes
BuildRequires:  pkgconfig
BuildRequires:  qt6-core-private-devel
BuildRequires:  qt6-gui-private-devel
BuildRequires:  qt6-qml-private-devel
BuildRequires:  qt6-quick-private-devel
BuildRequires:  qt6-shadertools-private-devel
BuildRequires:  cmake(Qt6Core)
BuildRequires:  cmake(Qt6Gui)
BuildRequires:  cmake(Qt6Network)
BuildRequires:  cmake(Qt6Quick)
BuildRequires:  cmake(Qt6ShaderTools)
BuildRequires:  cmake(Qt6Xml)
BuildRequires:  pkgconfig(icu-i18n)

%description
The Qt 6 Compat library provides unsupported Qt 5 APIs which can be useful
while porting Qt 5 code.

%package imports
Summary:        Qt 6 Core 5 Compat QML files and plugins

%description imports
QML files and plugins from the Qt 6 Core5Compat module.
This package shall be used while porting away from qtgraphicaleffects.

%package -n libQt6Core5Compat6
Summary:        Qt 6 Core 5 Compat library

%description -n libQt6Core5Compat6
The Qt 6 Core 5 Compat library

%package devel
Summary:        Qt 6 Core 5 Compat library - Development files
Requires:       libQt6Core5Compat6 = %{version}
Requires:       qt6-core-private-devel

%description devel
Development files for the Qt 6 Core 5 Compat library

%package private-devel
Summary:        Non-ABI stable API for the Qt 6 Qt5 Compat library
Requires:       cmake(Qt6Core5Compat) = %{real_version}
%requires_eq    qt6-core-private-devel

%description private-devel
This package provides private headers of libQt6Core5Compat that do not have any
ABI or API guarantees.


%prep
%autosetup -p1 -n %{tar_name}-%{real_version}%{tar_suffix}

%build
%cmake_qt6

%{qt6_build}

%install
%{qt6_install}

%fdupes %{buildroot}

# CMake files are not needed for plugins
rm -r %{buildroot}%{_qt6_cmakedir}/Qt6Qml/QmlPlugins

%post -n libQt6Core5Compat6 -p /sbin/ldconfig
%postun -n libQt6Core5Compat6 -p /sbin/ldconfig

%files imports
%{_qt6_qmldir}/Qt5Compat/

%files -n libQt6Core5Compat6
%license LICENSES/*
%{_qt6_libdir}/libQt6Core5Compat.so.*

%files devel
%{_qt6_cmakedir}/Qt6/FindWrapIconv.cmake
%{_qt6_cmakedir}/Qt6BuildInternals/StandaloneTests/Qt5CompatTestsConfig.cmake
%{_qt6_cmakedir}/Qt6Core5Compat/
%{_qt6_descriptionsdir}/Core5Compat.json
%{_qt6_includedir}/QtCore5Compat/
%{_qt6_libdir}/libQt6Core5Compat.prl
%{_qt6_libdir}/libQt6Core5Compat.so
%{_qt6_metatypesdir}/qt6core5compat_*_metatypes.json
%{_qt6_mkspecsdir}/modules/qt_lib_core5compat.pri
%{_qt6_pkgconfigdir}/Qt6Core5Compat.pc
%exclude %{_qt6_includedir}/QtCore5Compat/%{real_version}

%files private-devel
%{_qt6_includedir}/QtCore5Compat/%{real_version}/
%{_qt6_mkspecsdir}/modules/qt_lib_core5compat_private.pri

%changelog
* Mon Oct 24 2022 Jingwiw <wangjingwei@iscas.ac.cn> - 6.4.0-1
- init package
